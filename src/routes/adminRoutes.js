const express = require('express');
const adminRouter = express.Router();
const { MongoClient } = require('mongodb');


const bookList = [
  {
    title: 'War and Peace',
    genre: 'Fiction',
    author: 'Victor Hugo',
    read: false
  },
  {
    title: 'Les Miserables',
    genre: 'Plays',
    author: 'Levi Kansikist',
    read: true
  }
]


function router(nav) {

  adminRouter.route('/')
    .get((req, res) => {

      //mongo stuff
      const url = 'mongodb://localhost:27017/';
      const dbName = 'libraryApp';

      (async function mongo() {
        let client; 
        try {
          client = await MongoClient.connect(url,{             
            useNewUrlParser : true
          });
          
          // client = await MongoClient.connect(url);

          const db = client.db(dbName);

          const response = await db.collection('books');

          // res.json(response);
          console.log(response);

          // await console.log();

          
        }catch(err){
          console.log(err);
        }
        client.close();
      }());

 
    })
  return adminRouter;
}

module.exports = router;