const express = require('express');
const bookRouter = express.Router();

function router(nav) {

  const bookList = [
    {
      title: 'War and Peace',
      genre: 'Fiction',
      author: 'Victor Hugo',
      read: false
    },
    {
      title: 'Les Miserables',
      genre: 'Plays',
      author: 'Levi Kansikist',
      read: true
    }
  ]


  bookRouter.route('/')
    .get((req, res) => {
      res.render('bookListView',
        {
          nav,
          title: 'Library',
          bookList
        }
      );
    })

  bookRouter.route('/:id')
    .get((req, res) => {
      const { id } = req.params;
      res.render('bookView',
        {
          nav,
          title: 'Library',
          book: bookList[id]
        });
    })

    return bookRouter;
}

module.exports = router;